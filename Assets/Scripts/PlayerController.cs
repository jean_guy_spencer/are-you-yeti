﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : Controller
{
    public bool yetiForm;
    public float characterSpeed;
    public float dashSpeed;
    public float dashTime;
    public float invincibleTime;
    public int maxAmmo;
    public Material snowballColor;
    public GameObject characterRenderer;
    public GameObject snowballPrefab;
    public Transform throwingHand;

    public AudioClip[] ouchSounds;
    public AudioClip[] throwSounds;

    public PlayerStat m_Ammo;
    public AmmoUI m_Ammo_UI;

    public PlayerStat m_Health;
    public ImageFill m_Health_UI;

    public PlayerStat m_DashTimer;
    public ImageFill m_DashTimer_UI;

    public int playerOriginalHealth { get; private set; }

    GameManager GM;
    Vector3 forward, right;
    Animation anim;
    Renderer rend;
    AudioSource au;

    float invincibleCooldownTimer;
    bool canMove = true;
    bool dashing = false;
    bool digging = false;
    bool casting = false;
    public bool invincible { get; private set; }

    new void Awake()
    {
        
        GM = GameObject.Find("Main Camera").GetComponent<GameManager>();
        base.Awake();
        anim = GetComponent<Animation>();
        au = GetComponent<AudioSource>();
        m_Health = new PlayerStat(m_Health_UI);
        m_Ammo = new PlayerStat(m_Ammo_UI);
        m_DashTimer = new PlayerStat(m_DashTimer_UI);
        m_DashTimer.SetMax(dashTime);
        m_DashTimer.SetCurrent(dashTime);
    }

    void Start()
    {
        invincible = false;
        
        forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
    }

    // Update is called once per frame
    void Update()
    {
        //if (GM.IsPaused)
        //    return;

        if (canMove)
        {
            
            if (Input.GetKeyDown(BUTTON_B) && !dashing)
                StartCoroutine(Dash());

            HandleMovement();

            if (yetiForm)
            {
                HandleYetiInput();
                //return;
            }

            else
                HandlePlayerInput();
        }

        else if (!yetiForm)
        {
            if (Input.GetKeyUp(BUTTON_X))
            {
                FinishMakingSnowballs();
            }
        }


        if (invincible)
        {
            if (characterRenderer.activeSelf)
                characterRenderer.SetActive(false);
            else if (!characterRenderer.activeSelf)
                characterRenderer.SetActive(true);

            invincibleCooldownTimer += Time.deltaTime;

            if (invincibleCooldownTimer >= invincibleTime)
            {
                EndInvincibility();
            }
        }

        if (dashing)
        {
            m_DashTimer.Adjust(Time.deltaTime);
            if (m_DashTimer.current >= m_DashTimer.max)
            {
                dashing = false;
                
                //emit particle??
            }
        }
    }

    void HandleMovement()
    {
        float x = Input.GetAxis(LEFT_STICK_HORIZ);
        float y = Input.GetAxis(LEFT_STICK_VERT) * -1;
        Vector3 direction = new Vector3(x, 0, y);

        if (direction.magnitude > 0.19f)
        {
            Vector3 rightMovement = right * characterSpeed * Time.deltaTime * x;
            Vector3 upMovement = forward * characterSpeed * Time.deltaTime * y;

            Vector3 heading = Vector3.Normalize(rightMovement + upMovement);
            transform.forward = heading;
            transform.position += rightMovement;
            transform.position += upMovement;

            if (!anim.IsPlaying("Run") && !anim.IsPlaying("Dash"))
                anim.Play("Run");
        }

        else if (casting)
        {
            if (!anim.IsPlaying("Summon"))
                anim.Play("Summon");
        }

        else if (!anim.IsPlaying("Idle") && !anim.IsPlaying("Dash"))
            anim.Play("Idle");
    }

    void HandlePlayerInput()
    {
        if (Input.GetKeyUp(BUTTON_X))
        {
            FinishMakingSnowballs();
        }

        if (Input.GetKeyDown(BUTTON_A))
            StartCoroutine(Throw());

        if (Input.GetKey(BUTTON_X))
        {
            if (m_Ammo.current < m_Ammo.max)
                BeginMakingSnowball();

            // else feedback??
        }
    }

    void HandleYetiInput()
    {
        if (Input.GetKeyDown(BUTTON_A))
            StartCoroutine(YetiAttack());

    }

    IEnumerator YetiAttack()
    {
        EnableMovement(false);
        anim.Play("Attack");

        float waitTime = anim["Attack"].length;
        yield return new WaitForSeconds(waitTime);

        if (!anim.IsPlaying("Death"))
            EnableMovement(true);
    }

    public void StorePlayerOriginalHealth(int health)
    {
        playerOriginalHealth = health;
    }

    public void RevertToCharacter(int newHealth)
    {
        m_Health.SetCurrent(newHealth);
    }

    void BeginMakingSnowball()
    {
        if (digging)
            return;

        EnableMovement(false);
        anim.Play("Gather");
        digging = true;
    }

    void FinishMakingSnowballs()
    {
        if (!digging)
            return;

        anim.Stop("Gather");
        digging = false;
        EnableMovement(true);
    }

    void ReceiveSnowball()
    {
        if (m_Ammo.current < m_Ammo.max)
        {
            m_Ammo.Adjust(1);
            // visual feedback
        }

        if (m_Ammo.current >= m_Ammo.max)
            FinishMakingSnowballs();
    }

    IEnumerator Dash()
    {
        m_DashTimer.SetCurrent(0);
        GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * dashSpeed, ForceMode.VelocityChange);
        dashing = true;
        anim.Play("Dash");
        yield return new WaitForSeconds(anim["Dash"].length);
    }

    IEnumerator Throw()
    {
        if (m_Ammo.current > 0)
        {
            PlaySound(throwSounds[Random.Range(0, throwSounds.Length - 1)]);
            m_Ammo.Adjust(-1);
            EnableMovement(false);
            GameObject snowball = Instantiate(snowballPrefab, throwingHand.position, transform.rotation) as GameObject;
            snowball.GetComponent<Renderer>().material = snowballColor;
            Physics.IgnoreCollision(GetComponent<Collider>(), snowball.GetComponent<Collider>());
            anim.Play("Throw");
            float waitTime = anim["Throw"].length;
            yield return new WaitForSeconds(waitTime);
            if (!anim.IsPlaying("Death"))
                EnableMovement(true);

        }
    }

    public void EnableMovement(bool toggle)
    {
        canMove = toggle;
    }

    void OnCollisionEnter(Collision other)
    {
        if (invincible)
            return;

        if (other.gameObject.tag == "SnowBall" || other.gameObject.tag == "Yama")
            StartCoroutine(Hit());
    }

    IEnumerator Hit()
    {
        if (digging)
            FinishMakingSnowballs();

        StartInvincibility();
        PlaySound(ouchSounds[Random.Range(0, ouchSounds.Length - 1)]);
        WorshipIdol idol = GameObject.Find("Worship Statue").GetComponent<WorshipIdol>();
        idol.ResetSummon();
        m_Health.Adjust(-1);
        digging = false;
        EnableMovement(false);
        anim.Play("Death");
        yield return new WaitForSeconds(anim["Death"].length + 2.0f);

        if (m_Health.current <= 0) Die();
        else EnableMovement(true);
    }

    void StartInvincibility()
    {
        invincible = true;
        this.gameObject.layer = 11;
    }

    void EndInvincibility()
    {
        this.gameObject.layer = 0;
        invincible = false;
        invincibleCooldownTimer = 0;
        characterRenderer.SetActive(true);
    }

    void Die()
    {
        this.gameObject.layer = 0;
        Destroy(this.gameObject);
    }

    void PlaySound(AudioClip clip)
    {
        au.clip = clip;
        au.Play();
    }

    public void BeginSummom()
    {
        casting = true;
    }

    public void StopSummoning()
    {
        casting = false;
    }
}
