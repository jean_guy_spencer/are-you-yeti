﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class MenuManager : MonoBehaviour {

    private Menu CurrentMenu;
    public Menu TitleSplash;
    public Menu MainMenu;
    public Menu PauseMenu;

    void Start()
    {
        ShowTitleMenu();
    }

    public void ShowTitleMenu() { ShowMenu(TitleSplash); }
    public void ShowMainMenu() { ShowMenu(MainMenu); }
    public void ShowPauseMenu() { ShowMenu(PauseMenu); }

    public void ShowMenu(Menu menu)
    {
        if (CurrentMenu != null)
            CurrentMenu.IsOpen = false;

        CurrentMenu = menu;
        EventSystem.current.SetSelectedGameObject(CurrentMenu.defaultSelection);
        CurrentMenu.IsOpen = true;
    }

    public void CloseCurrentMenu()
    {
        if (CurrentMenu != null)
        {
            CurrentMenu.IsOpen = false;
            CurrentMenu = null;
        }
    }
}
