﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(Image))]
public class ImageFill : UIScript {
    public float lowerMargin, upperMargin;

    public override void UpdateUI()
    {
        float current = (float)m_Owner.current;
        float max = (float)m_Owner.max;
        float newValue = (current * (upperMargin - lowerMargin)) / max;
        newValue += lowerMargin;
        newValue = Mathf.Clamp(newValue, lowerMargin, upperMargin);
        
        Image image = this.gameObject.GetComponent<Image>();
        image.fillAmount = newValue;
    }
}
