﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoUI : UIScript {
    public Image[] snowballs;

    public override void UpdateUI()
    {
        for(int i = 0; i < m_Owner.max; i++)
        {
            if (i < m_Owner.current)
                snowballs[i].enabled = true;
            else
                snowballs[i].enabled = false;
        }
    }
}
