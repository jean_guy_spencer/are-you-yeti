﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SmashCam : MonoBehaviour
{

    public GameObject[] objects;
    public float camSpeed;
    Camera cam;
    public float maxDistance, minDistance;

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();

    }

    // Update is called once per frame
    void Update()
    {
        objects = GameObject.FindGameObjectsWithTag("Player");

        bool zoomOut = false;
        bool zoomIn = false;

        foreach (GameObject g in objects)
        {
            Vector3 pos = cam.WorldToViewportPoint(g.transform.position);

            // check for chance to zoom out
            if (pos.x < 0.2 || pos.x > 0.8 || pos.y < 0.2 || pos.y > 0.8)
                zoomOut = true;

            // check for chance to zoom in
            else if ((pos.x > 0.3 && pos.x < 0.7) || (pos.y > 0.3 && pos.y < 0.7))
                zoomIn = true;

            else
            {
                zoomIn = false;
                zoomOut = false;
            }
        }

        if (zoomOut)
        {
            if (cam.transform.position.z >= maxDistance)
                cam.transform.Translate(Vector3.back * Time.deltaTime * camSpeed);

            else
                zoomOut = false;
        }

        else if (zoomIn)
        {
            if (cam.transform.position.z <= minDistance)
                cam.transform.Translate(Vector3.forward * Time.deltaTime * camSpeed);

            else
                zoomIn = false;
        }


    }
}
