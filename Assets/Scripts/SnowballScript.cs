﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
[RequireComponent (typeof(SphereCollider))]

public class SnowballScript : MonoBehaviour {
    public float speed;
    public float maxTime;
	public ParticleSystem snowballHit;
    float lifeTimer;  

	// Use this for initialization
	void Start () {
        lifeTimer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        lifeTimer += Time.deltaTime;
        transform.Translate(speed * Vector3.forward * Time.deltaTime);

        if (lifeTimer > maxTime)
            Destroy(gameObject);
	}

    void OnCollisionEnter(Collision coll)
    {
		if(coll.gameObject.tag == "Player")
		{
			if(coll.gameObject.GetComponent<PlayerController>().invincible)
				return;
		}

		GetComponent<Collider>().enabled = false;
		StartCoroutine(SnowBallHit());
    }

	IEnumerator SnowBallHit()
	{
		//Spawn particle
		GameObject.Instantiate(snowballHit, this.gameObject.transform.position, this.gameObject.transform.rotation);

		// make snowball invisible (to seem as if it was destroyed)
		GetComponent<Renderer>().enabled = false;

		// Play sound and wait until it's done before actually destroying this object
		AudioSource au = GetComponent<AudioSource>();
		au.Play();
		yield return new WaitForSeconds(au.clip.length);
		Destroy(gameObject);
	}
}
