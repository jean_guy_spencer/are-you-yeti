﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UIScript : MonoBehaviour {
    public PlayerStat m_Owner { get; private set; }

    public void SetOwner(PlayerStat owner)
    {
        m_Owner = owner;
    }

    public virtual void UpdateUI() { }
}
