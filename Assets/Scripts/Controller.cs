﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Character controller.
/// Attach this to an object you want to control.
/// Player number must be assigned manually.
/// Call Button() from another script to receive input.
/// </summary>
public class Controller : MonoBehaviour {

	public int playerNumber;

    // Axes
    protected string LEFT_STICK_HORIZ;
    protected string LEFT_STICK_VERT;
    protected string RIGHT_STICK_HORIZ;
    protected string RIGHT_STICK_VERT;
    protected string DPAD_HORIZ;
    protected string DPAD_VERT;
    protected string TRIGGERS;

    // Buttons
    protected string BUTTON_A;
    protected string BUTTON_B;
    protected string BUTTON_X;
    protected string BUTTON_Y;
    protected string BUTTON_LEFT_BUMPER;
    protected string BUTTON_RIGHT_BUMPER;
    protected string BUTTON_LEFT_STICK_PRESS;
    protected string BUTTON_RIGHT_STICK_PRESS;
    protected string BUTTON_START;
    protected string BUTTON_BACK;
	
	protected void Awake () {
		DefineStrings();
	}

    public void SetPlayerNumber(int num)
    {
        playerNumber = num;
        DefineStrings();
    }

    /// <summary>
    /// Define axes strings based on player (joystick) number.
    /// See Input Manager in the Unity Editor.
    /// </summary>
    void DefineStrings()
	{
		LEFT_STICK_HORIZ = ("P" + playerNumber + "_LEFT_STICK_HORIZONTAL");
		LEFT_STICK_VERT = ("P" + playerNumber + "_LEFT_STICK_VERTICAL");
		RIGHT_STICK_HORIZ = ("P" + playerNumber + "_RIGHT_STICK_HORIZONTAL");
		RIGHT_STICK_VERT = ("P" + playerNumber + "_RIGHT_STICK_VERTICAL");
		DPAD_HORIZ = ("P" + playerNumber + "_DPAD_HORIZONTAL");
		DPAD_VERT = ("P" + playerNumber + "_DPAD_VERTICAL");
		TRIGGERS = ("P" + playerNumber + "_TRIGGERS");

        //BUTTON_A = ("P" + playerNumber + "_A");
        BUTTON_A = ("joystick " + playerNumber + " button 0");
		BUTTON_B = ("joystick " + playerNumber + " button 1");
		BUTTON_X = ("joystick " + playerNumber + " button 2");
		BUTTON_Y = ("joystick " + playerNumber + " button 3");
		BUTTON_LEFT_BUMPER = ("joystick " + playerNumber + " button 4");
		BUTTON_RIGHT_BUMPER = ("joystick " + playerNumber + " button 5");
		BUTTON_LEFT_STICK_PRESS = ("joystick " + playerNumber + " button 8");
		BUTTON_RIGHT_STICK_PRESS = ("joystick " + playerNumber + " button 9");
		BUTTON_START = ("joystick " + playerNumber + " button 7");
		BUTTON_BACK = ("joystick " + playerNumber + " button 6");
	}
}
