﻿using UnityEngine;
using System.Collections;

public class PlayerStat
{
    public float max { get; private set; }
    public float current { get; private set; }
    public UIScript m_UIScript;

    public PlayerStat()
    {
        max = 0;
        current = 0;
    }

    public PlayerStat(UIScript ui)
    {
        m_UIScript = ui;
        max = 0;
        current = 0;
    }

    public void SetMax(float newMax)
    {
        max = newMax;
        UpdateUI();
    }

    public void SetCurrent(float newCurrent)
    {
        if (newCurrent < 0)
        {
            Debug.LogWarning("Cannot set stats to negative value.");
            return;
        }

        current = newCurrent;
        UpdateUI();
    }

    public void Adjust(float adjustment)
    {
        if (current + adjustment < 0)
        {
            SetCurrent(0);
            return;
        }

        current += adjustment;
        UpdateUI();
    }

    public virtual void UpdateUI()
    {
        if (m_UIScript.m_Owner == null)
            m_UIScript.SetOwner(this);
        m_UIScript.UpdateUI();
    }
}
