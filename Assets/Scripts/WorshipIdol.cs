﻿using UnityEngine;
using System.Collections;

public class WorshipIdol : MonoBehaviour
{
    public GameManager gm;
    public float castingTime;
    public float summoningTime;
    public float timeBeforeActive;
    public ParticleSystem activeParticle;
    public ParticleSystem summoning;
    public ParticleSystem transformation;
    public Transform summonLocation;
    public GameObject yetiPrefab;
    public GameObject[] playerPrefabs;

    public AudioClip castingSound;
    public AudioClip summoningSound;
    public AudioClip revertSound;

    public GameObject yeti { get; private set; }
    public PlayerController castingPlayer { get; private set; }

    bool casting = false;
    bool platformActive = false;
    bool used = false;
    float activeTimer = 0;
    float castingTimer = 0;
    float summonTimer = 0;

    

    // Use this for initialization
    void Start()
    {
        Physics.IgnoreLayerCollision(11, 11);
        Physics.IgnoreLayerCollision(10, 11);
    }

    // Update is called once per frame
    void Update()
    {
        if (casting)
            castingTimer += Time.deltaTime;

        if (yeti != null)
        {
            summonTimer += Time.deltaTime;

            if (summonTimer >= summoningTime && yeti != null)
                RevertPlayer();
        }

        else if(!used)
        {
            activeTimer += Time.deltaTime;

            if (activeTimer >= timeBeforeActive)
                EnableIdol();
        }

        if (castingTimer >= castingTime)
            Transform();
    }

    void OnTriggerStay(Collider other)
    {
        if (!platformActive)
            return;

        if (other.gameObject.tag == "Player")
        {
            if (other.gameObject.GetComponent<PlayerController>().invincible)
            {
                ResetSummon();
                return;
            }

            if (castingPlayer != null)
                return;

            casting = true;
            castingPlayer = other.gameObject.GetComponent<PlayerController>();
            castingPlayer.BeginSummom();
            summoning.gameObject.SetActive(true);
            
            PlaySound(castingSound);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (!platformActive)
            return;

        if (other.gameObject.tag == "Player")
        {
            if (castingPlayer == other.gameObject.GetComponent<PlayerController>())
            {
                castingPlayer.StopSummoning();
                ResetSummon();
                EnableIdol();
            }
        }
    }

    public void ResetSummon()
    {
		summoning.gameObject.SetActive(false);
        StopSound();
        casting = false;
        if(castingPlayer != null)
            castingPlayer.StopSummoning();
        castingPlayer = null;
        activeTimer = 0;
        castingTimer = 0;
        summonTimer = 0;
    }

    void DisableIdol()
    {
        platformActive = false;
        activeParticle.gameObject.SetActive(false);
        summoning.gameObject.SetActive(false);
        ResetSummon();
    }

    public void EnableIdol()
    {
        platformActive = true;
        activeParticle.gameObject.SetActive(true);
    }

    void RevertPlayer()
    {
        //Particle
        GameObject.Instantiate(transformation, yeti.transform.position, yeti.transform.rotation);

        //Player
        PlayerController yetiScript = yeti.GetComponent<PlayerController>();
        int playerNum = yetiScript.playerNumber;
        GameObject newPlayer = GameObject.Instantiate(playerPrefabs[playerNum - 1], yeti.transform.position, yeti.transform.rotation) as GameObject;
        PlayerController playerScript = newPlayer.GetComponent<PlayerController>();
        
        //playerScript.health.SetCurrent(yetiScript.playerOriginalHealth);
        playerScript.SetPlayerNumber(playerNum);
        gm.SwitchModels(newPlayer);
        Destroy(yeti);
        yeti = null;

        PlaySound(revertSound);
        gm.PlayMusic(false);
    }

    void Transform()
    {
        //Particle
        GameObject.Instantiate(transformation, summonLocation.position, transformation.transform.rotation);

        //Yeti
        yeti = GameObject.Instantiate(yetiPrefab, summonLocation.position, yetiPrefab.transform.rotation) as GameObject;
        PlayerController yetiScript = yeti.GetComponent<PlayerController>();
        //democharacter playerScript = castingPlayer;

        yetiScript.StorePlayerOriginalHealth((int)castingPlayer.m_Health.current);
        yetiScript.SetPlayerNumber(castingPlayer.playerNumber);
        gm.SwitchModels(yeti);
        Destroy(castingPlayer.gameObject);
        DisableIdol();

        PlaySound(summoningSound);
        gm.PlayMusic(true);
    }

    void PlaySound(AudioClip clip)
    {
        AudioSource au = GetComponent<AudioSource>();
        au.clip = clip;
        au.Play();
    }

    void StopSound()
    {
        AudioSource au = GetComponent<AudioSource>();
        au.Stop();
    }
}
