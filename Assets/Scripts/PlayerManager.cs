﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(GameManager))]

public class PlayerManager : MonoBehaviour
{

    public GameManager gm { get; private set; }

    public GUISkin UIskin;
    public int maxPlayerHealth;
    public int maxYetiHealth;

    GameObject[] players;
    List<PlayerController> characterScripts;

    // Use this for initialization
    void Awake()
    {
        gm = this.gameObject.GetComponent<GameManager>();

        players = new GameObject[4];
        players[0] = GameObject.Find("Jenny");
        players[1] = GameObject.Find("Jeffery");
        players[2] = GameObject.Find("Wedgie");
        players[3] = GameObject.Find("Kendra");

        characterScripts = new List<PlayerController>();

        for (int i = 0; i < players.Length; i++)
            characterScripts.Add(players[i].GetComponent<PlayerController>());
    }

    void Start()
    {
        foreach (PlayerController p in characterScripts)
        {
            p.m_Health.SetMax(maxPlayerHealth);
            p.m_Health.SetCurrent(maxPlayerHealth);
            p.m_Ammo.SetMax(4);
            p.m_Ammo.SetCurrent(0);
        }
    }

    public void SwitchModels(GameObject newObject)
    {
        int playerIndex = newObject.GetComponent<Controller>().playerNumber - 1;
        int newMaxHealth = maxYetiHealth;
        int newCurrentHealth = maxYetiHealth;

        if (newObject.tag == "Player")
        {
            newMaxHealth = maxPlayerHealth;
            newCurrentHealth = characterScripts[playerIndex].GetComponent<PlayerController>().playerOriginalHealth;
        }

        players[playerIndex] = newObject;
        characterScripts[playerIndex] = newObject.GetComponent<PlayerController>();
        characterScripts[playerIndex].m_Health.SetMax(newMaxHealth);
        characterScripts[playerIndex].m_Health.SetCurrent(newCurrentHealth);
    }

    public void AllowMovement(bool toggle)
    {
        foreach (GameObject g in players)
            g.GetComponent<PlayerController>().EnableMovement(toggle);
    }

    // Update is called once per frame
    void Update()
    {

    }

    //void OnGUI()
    //{
    //    GUI.skin = UIskin;
    //    GUI.skin.box.fontSize = Screen.height / 30;

    //    for (int i = 0; i < characterScripts.Count; i++)
    //    {
    //        if (!characterScripts[i])
    //            continue;
    //        PlayerController d = characterScripts[i];
    //        string playerInfo = (d.gameObject.name + "\nHealth: " + d.m_Health.current + "/" + d.m_Health.max + "\nAmmo: " + d.m_Ammo.current + "/" + d.m_Ammo.max);
    //        GUI.Box(new Rect(0 + Screen.width / players.Length * i, Screen.height - Screen.height / 7, Screen.width / players.Length, Screen.height / 7),
    //            playerInfo);
    //    }
    //}
}
