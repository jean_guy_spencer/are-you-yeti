﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PlayerManager))]

public class GameManager : MonoBehaviour
{
    public PlayerManager pm { get; private set; }
    public float globalTimer { get; private set; }
    public GameState gameState { get; private set; }
    public UIManager UI { get; private set; }
    public bool IsPaused { get; private set; }
    public float maxGameTime;
    public AudioClip musicNormal;
    public AudioClip musicYeti;
    public MenuManager _menuManager;
    

    // Use this for initialization
    void Awake()
    {
        
        IsPaused = true;
        Time.timeScale = 0;
        UI = GameObject.Find("UI Canvas").GetComponent<UIManager>();
        pm = this.gameObject.GetComponent<PlayerManager>();
        gameState = GameState.TITLE;
        globalTimer = maxGameTime;
        Physics.IgnoreLayerCollision(9, 10);
    }

    void Start()
    {
        AllowMovement(false);
    }

    // Update is called once per frame
    void Update()
    {
        switch (gameState)
        {
            case GameState.TITLE:
                if (Input.GetButtonDown("PAUSE"))
                {
                    gameState = GameState.MAIN_MENU;
                    _menuManager.ShowMainMenu();
                }
                break;

            case GameState.MAIN_MENU:
                break;

            case GameState.START_COUNTDOWN:
                break;

            case GameState.PLAY:
                //globalTimer -= Time.deltaTime;
                if (globalTimer >= maxGameTime)
                    //MatchEnd();

                if (Input.GetButtonDown("PAUSE"))
                {
                    Pause();
                }

                break;

            case GameState.PAUSE:
                if (Input.GetButtonDown("PAUSE"))
                {
                    Resume();
                }
                
                // show pause menu
                // controls?
                break;

            case GameState.TIMER_END:
                // 
                break;

            case GameState.GAMEOVER:
                break;

            case GameState.RESULTS:
                break;

            default:
                break;
        }


    }

    public void SwitchModels(GameObject newObject)
    {
        pm.SwitchModels(newObject);
    }

    public void Pause()
    {
        IsPaused = true;
        AllowMovement(false);
        gameState = GameState.PAUSE;
        _menuManager.ShowPauseMenu();
        Time.timeScale = 0;
    }

    public void Resume()
    {
        
        IsPaused = false;
        _menuManager.CloseCurrentMenu();
        gameState = GameState.PLAY;
        Time.timeScale = 1;
        AllowMovement(true);
    }

    public void BeginPlay()
    {
        //start timer countdown
        Resume();
        PlayMusic(false);
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }

    void MatchEnd()
    {
        AllowMovement(false);
        gameState = GameState.TIMER_END;
    }

    void AllowMovement(bool toggle)
    {
        pm.AllowMovement(toggle);
    }

    public void PlayMusic(bool yeti)
    {
        AudioSource au = GetComponent<AudioSource>();
        au.Stop();
        if (yeti)
            au.clip = musicYeti;
        else
            au.clip = musicNormal;

        au.Play();
    }

    void OnGUI()
    {
        GUILayout.Label(globalTimer.ToString());

        switch (gameState)
        {
            case GameState.PAUSE:
                GUILayout.Label("PAUSED");
                break;
        }
    }
}
