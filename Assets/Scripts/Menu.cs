﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(Animator))]
public class Menu : MonoBehaviour
{
    public GameObject defaultSelection;
    private Animator _animator;
    private CanvasGroup _canvasGroup;
    public bool IsOpen
    {
        get { return _animator.GetBool("IsOpen"); }
        set { _animator.SetBool("IsOpen", value); }
    }

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _canvasGroup = GetComponent<CanvasGroup>();

        var rect = GetComponent<RectTransform>();
        rect.offsetMax = rect.offsetMin = Vector2.zero;
    }

    void Update()
    {
        if (!_animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
            _canvasGroup.blocksRaycasts = _canvasGroup.interactable = false;
        else
            _canvasGroup.blocksRaycasts = _canvasGroup.interactable = true;
    }
}
