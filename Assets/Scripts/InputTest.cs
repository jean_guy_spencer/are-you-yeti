﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Plug in up to four Xbox Controllers and attach this
/// to the Main Camera for testing
/// </summary>
public class InputTest : MonoBehaviour {

	int playerNumber;
	int numberOfJoysticks;

	void OnGUI()
	{
		numberOfJoysticks = Input.GetJoystickNames().Length;

		if(numberOfJoysticks == 0)
			return;

		for(playerNumber = 1; playerNumber <= numberOfJoysticks; playerNumber++)
		{
			GUILayout.Label(Input.GetJoystickNames()[playerNumber-1]);
			
			if (Mathf.Abs(Input.GetAxis("P" + playerNumber + "_LEFT_STICK_HORIZONTAL")) > 0.2F )
				GUILayout.Label("P" + playerNumber + " LEFT STICK HORIZONTAL: " + Input.GetAxis("P" + playerNumber + "_LEFT_STICK_HORIZONTAL"));
			
			if (Mathf.Abs(Input.GetAxis("P" + playerNumber + "_LEFT_STICK_VERTICAL")) > 0.2F )
				GUILayout.Label("P" + playerNumber + " LEFT STICK VERTICAL: " + Input.GetAxis("P" + playerNumber + "_LEFT_STICK_VERTICAL"));

			if (Mathf.Abs(Input.GetAxis("P" + playerNumber + "_RIGHT_STICK_HORIZONTAL")) > 0.2F )
				GUILayout.Label("P" + playerNumber + " RIGHT STICK HORIZONTAL: " + Input.GetAxis("P" + playerNumber + "_RIGHT_STICK_HORIZONTAL"));

			if (Mathf.Abs(Input.GetAxis("P" + playerNumber + "_RIGHT_STICK_VERTICAL")) > 0.2F )
				GUILayout.Label("P" + playerNumber + " RIGHT STICK VERTICAL: " + Input.GetAxis("P" + playerNumber + "_RIGHT_STICK_VERTICAL"));

			if (Mathf.Abs(Input.GetAxis("P" + playerNumber + "_DPAD_HORIZONTAL")) > 0.001F )
				GUILayout.Label("P" + playerNumber + " D-PAD HORIZONTAL: " + Input.GetAxis("P" + playerNumber + "_DPAD_HORIZONTAL"));

			if (Mathf.Abs(Input.GetAxis("P" + playerNumber + "_DPAD_VERTICAL")) > 0.001F )
				GUILayout.Label("P" + playerNumber + " D-PAD VERTICAL: " + Input.GetAxis("P" + playerNumber + "_DPAD_VERTICAL"));

			if (Input.GetAxis("P" + playerNumber + "_TRIGGERS") > 0.01F )
				GUILayout.Label("P" + playerNumber + " LEFT TRIGGER " + Input.GetAxis("P" + playerNumber + "_TRIGGERS"));

			if (Input.GetAxis("P" + playerNumber + "_TRIGGERS") < -0.01F )
				GUILayout.Label("P" + playerNumber + " RIGHT TRIGGER " + Input.GetAxis("P" + playerNumber + "_TRIGGERS"));
			
			if(Input.GetButton("P" + playerNumber + "_A"))
				GUILayout.Label("P" + playerNumber + " A");

			if(Input.GetButton("P" + playerNumber + "_B"))
				GUILayout.Label("P" + playerNumber + " B");

			if(Input.GetButton("P" + playerNumber + "_X"))
				GUILayout.Label("P" + playerNumber + " X");

			if(Input.GetButton("P" + playerNumber + "_Y"))
				GUILayout.Label("P" + playerNumber + " Y");

			if(Input.GetButton("P" + playerNumber + "_LEFT_BUMPER"))
				GUILayout.Label("P" + playerNumber + " LB");

			if(Input.GetButton("P" + playerNumber + "_RIGHT_BUMPER"))
				GUILayout.Label("P" + playerNumber + " RB");

			if(Input.GetButton("P" + playerNumber + "_START"))
				GUILayout.Label("P" + playerNumber + " START");

			if(Input.GetButton("P" + playerNumber + "_BACK"))
				GUILayout.Label("P" + playerNumber + " BACK");

			if(Input.GetButton("P" + playerNumber + "_LEFT_STICK_PRESS"))
				GUILayout.Label("P" + playerNumber + " LSPress");

			if(Input.GetButton("P" + playerNumber + "_RIGHT_STICK_PRESS"))
				GUILayout.Label("P" + playerNumber + " RSPress");

		}
	}
}
